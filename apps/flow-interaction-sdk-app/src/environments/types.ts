import { Provider } from '@angular/core';
import { Item } from '@backbase/foundation-ang/core';
import { ExternalServices } from '@backbase/web-sdk-api-ang';

export interface Environment {
  readonly production: boolean;
  readonly mockProviders: Array<Provider>;
  readonly bootstrap?: {
    readonly pageModel: Item;
    readonly services: ExternalServices;
  };
}
