// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { Environment } from './types';

const services = {};
const pageModel = {
  name: 'bb-flow-interaction-reference-ang',
  properties: {},
  children: [
    {
      name: 'bb-flow-app-ang',
      properties: {
        classId: 'FlowInteractionContainerComponent',
        outletName: '',
        interactionName: 'example-journey',
        'input.interactionName': 'model interactionName',
        apiVersion: 'v1',
        'input.apiVersion': 'model apiVersion',
        servicePath: 'example-journey-service/api/interaction',
        'input.servicePath': 'model servicePath',
        deploymentPath: 'interactions',
        'input.deploymentPath': 'model deploymentPath',
      },
      children: [
        {
          name: 'personal-details',
          properties: {
            classId: 'PersonalDetailsComponent',
            route: 'personal-details',
          },
        },
        {
          name: 'mobile-verification',
          properties: {
            classId: 'MobileVerificationComponent',
            route: 'mobile-verification',
          },
        },
      ],
    },
  ],
};

export const environment: Environment = {
  production: false,
  mockProviders: [],
  bootstrap: {
    pageModel,
    services,
  },
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
