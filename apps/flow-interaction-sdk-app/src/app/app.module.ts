import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { FlowInteractionCoreModule } from '@backbase/flow-interaction-core-ang';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { WebSdkApiModule } from '@backbase/foundation-ang/web-sdk';
import { RouterModule } from '@angular/router';
import { FlowInteractionContainerModule } from '@backbase/flow-interaction-container-ang';
import { PersonalDetailsComponent } from './personal-details/personal-details.component';
import { MobileVerificationComponent } from './mobile-verification/mobile-verification.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [AppComponent, PersonalDetailsComponent, MobileVerificationComponent],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot([], {
      useHash: true,
      initialNavigation: false,
    }),
    FlowInteractionCoreModule.forRoot({
      steps: {
        'personal-details': ['personal-details'],
        'mobile-verification': ['mobile-verification'],
      },
      actionConfig: {
        'submit-personal-details': {
          updateCdo: true,
        },
        'request-verification-sms-code': {},
        'check-verification-code': {
          updateCdo: true,
        },
      },
    }),
    FlowInteractionContainerModule,
    WebSdkApiModule,
    BackbaseCoreModule.forRoot({
      classMap: {
        PersonalDetailsComponent,
        MobileVerificationComponent,
      },
    }),
  ],
  providers: [...environment.mockProviders],
  bootstrap: [AppComponent],
})
export class AppModule {}
