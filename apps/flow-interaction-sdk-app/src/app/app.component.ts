import { Component } from '@angular/core';

@Component({
  selector: 'bb-flow-interaction-sdk-app',
  template: `<bb-root></bb-root>`,
})
export class AppComponent {
  constructor() {}
}
