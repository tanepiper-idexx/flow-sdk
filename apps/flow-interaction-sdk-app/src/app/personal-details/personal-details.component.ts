import { Component, OnInit } from '@angular/core';
import { FlowInteractionService } from '@backbase/flow-interaction-core-ang';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'bb-personal-details',
  template: `
      <form [formGroup]="userForm" (ngSubmit)="submit()">
          <div class="form-control">
              <label for="first-name">First Name:</label>
              <input id="first-name" type="text" formControlName="firstName"
                     placeholder="Please enter your first name"/>
          </div>
          <div class="form-control">
              <label for="last-name">Last Name:</label>
              <input id="last-name" type="text" formControlName="lastName" placeholder="Please enter your last name"/>
          </div>
          <button class="btn btn-primary" type="submit">Submit</button>
      </form>
  `,
  styles: [],
})
export class PersonalDetailsComponent implements OnInit {
  userForm = this.fb.group({
    firstName: [''],
    lastName: [''],
  });

  constructor(private readonly flow: FlowInteractionService, private readonly fb: FormBuilder) {}

  ngOnInit() {}

  submit() {
    const { firstName, lastName } = this.userForm.value;

    this.flow
      .dispatch({
        action: 'submit-personal-details',
        body: {
          payload: { firstName, lastName },
        },
      })
      .subscribe(result => {
        return this.flow.nav.next();
      });
  }
}
