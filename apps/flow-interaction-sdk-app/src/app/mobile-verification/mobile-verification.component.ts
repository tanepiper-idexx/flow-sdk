import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FlowInteractionService } from '@backbase/flow-interaction-core-ang';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'bb-mobile-verification',
  template: `
      <button (click)="requestToken()">Click Me</button>
  `,
  styles: [],
})
export class MobileVerificationComponent implements OnInit {
  tokenRequested$ = new BehaviorSubject<boolean>(false);
  verifyForm = this.fb.group({
    otpCode: [''],
  });

  constructor(private readonly flow: FlowInteractionService, private readonly fb: FormBuilder) {}

  ngOnInit() {}

  requestToken() {
    this.flow
      .call({
        action: 'request-verification-sms-code',
        body: {},
      })
      .subscribe(result => {
        console.log(result);
      });
  }
}
