"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var core_2 = require("@backbase/foundation-ang/core");
var app_module_1 = require("./app/app.module");
var environment_1 = require("./environments/environment");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
core_2.registerSingleApp(function (extraProviders) {
    return platform_browser_dynamic_1.platformBrowserDynamic(extraProviders).bootstrapModule(app_module_1.AppModule);
});
//# sourceMappingURL=main.js.map