# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

> Note: Due to the way packages are bumped there may be skipped versions in here, if so
> then no new feature was released on that module.

## [0.14.0] - 2019-08-20

Previous versions have many bug fixes

### Added

- Supports Page Config API root being passed that contains an additional slash [`/`] at the end

## [0.11.0] - 2019-08-16

- Changed `gotoNextStep` method to `next` - call this method after a dispatch/call to go to the next nav event,
or to handle the status on the current page

## [0.9.0] - 2019-08-13

### Added

- Support for `.withConfig` option on modules to provide a way to override default `STEP_CONFIGURATION` options provided with a step

## [0.7.0] - 2019-08-13

Internal refactor of library

## [0.6.0] - 2019-08-07

### Added

- New `poll` method added to core that provides the ability to poll and endpoint and awaiting for a nav change from an async event
- Better support for using an `Observer` within components for error handling
- `FlowInteractionInterceptor` for handling flow-based application errors

## [0.5.0] - 2019-08-02

### Fixed

- No longer send interactionId if not set

### Changed

- `getInteractionData` returns a full interaction response

## [0.4.0] - 2019-07-26

### Added

- Unit tests added to code base

## [0.3.0] - 2019-07-26

### Added

- Ensure interaction ID is set with each request, so not required from outside services

## [0.2.0] - 2019-07-25

### Changed

- Added better documentation
- Export types manually

## [0.1.0] - 2019-07-25

Initial Release
