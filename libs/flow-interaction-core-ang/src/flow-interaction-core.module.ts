import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FlowInteractionService } from './services/interaction.service';
import { FlowNavigationService } from './services/navigation.service';
import { createActionMap, createStepRouteMap } from './utils/config';
import { FlowApiService } from './services/api-service.service';
import { ACTION_MAP_FACTORY, FLOW_CONFIGURATION, FlowInteractionConfig, STEP_MAP_FACTORY } from './types/config';
import { FlowCustomerDataObjectService } from './services/customer-data-object.service';

/**
 * The Flow Interaction Core Module provides the service for interaction,
 * and imports additional services for nav and errors
 */
@NgModule({
  imports: [HttpClientModule],
  providers: [
    FlowInteractionService,
    FlowNavigationService,
    FlowApiService,
    FlowCustomerDataObjectService,
    {
      provide: FLOW_CONFIGURATION,
      useValue: {
        steps: {},
        actionConfig: {},
      },
    },
    {
      provide: STEP_MAP_FACTORY,
      useFactory: createStepRouteMap,
      deps: [FLOW_CONFIGURATION],
    },
    {
      provide: ACTION_MAP_FACTORY,
      useFactory: createActionMap,
      deps: [FLOW_CONFIGURATION],
    },
  ],
})
export class FlowInteractionCoreModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: FlowInteractionCoreModule,
  ) {}

  /**
   * The `forRoot` method allows the configuration of an interaction
   * @param config
   */
  static forRoot(config: FlowInteractionConfig): ModuleWithProviders {
    return {
      ngModule: FlowInteractionCoreModule,
      providers: [
        {
          provide: FLOW_CONFIGURATION,
          useValue: config,
        },
      ],
    };
  }
}
