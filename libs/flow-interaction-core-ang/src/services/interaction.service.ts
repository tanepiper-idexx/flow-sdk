import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, throwError, timer } from 'rxjs';
import { catchError, filter, pluck, switchMap, takeWhile, tap } from 'rxjs/operators';
import { CustomerDataObject, InteractionRequest, InteractionName, ServiceConfig } from '../types/interaction';
import { FlowNavigationService } from './navigation.service';
import { InteractionResponse } from '../types/response';
import { FlowApiService } from './api-service.service';
import { ActionMap } from '../utils/config';
import { ACTION_MAP_FACTORY, ActionConfig } from '../types/config';
import { FlowCustomerDataObjectService } from './customer-data-object.service';

/**
 * The Flow Interaction service is the core service provided to Journeys. This service provides
 * the API to interact with the Interaction Engine, and subscribe to a stream of actions for a particular
 * `interactionId`
 */
@Injectable()
export class FlowInteractionService<CDO = CustomerDataObject> {
  /**
   * Sending state
   */
  private readonly sending$ = new BehaviorSubject<boolean>(false);

  /**
   * The Flow Interaction Service uses the {FlowInteractionDataService}
   * @param actionMap the Action map provided by configuration
   * @param nav The Interaction Navigation service
   * @param cdo The Interaction Customer Data Object service
   * @param api The Interaction API service
   */
  constructor(
    @Inject(ACTION_MAP_FACTORY) private readonly actionMap: ActionMap,
    public readonly nav: FlowNavigationService,
    public readonly cdo: FlowCustomerDataObjectService<CDO>,
    private readonly api: FlowApiService,
  ) {}

  /**
   * Observable value of the current HTTP sending state
   */
  public get isSending(): Observable<boolean> {
    return this.sending$.asObservable();
  }

  /**
   * Initialise the service with a service config
   * @param serviceConfig
   * @param interactionName
   */
  public init(serviceConfig: ServiceConfig, interactionName: InteractionName) {
    this.sending$.next(false);
    this.api.init(serviceConfig, interactionName);
  }

  /**
   * Dispatch sends an interaction request with customer data to the service to update the customer data object
   * @param request The Action to set to the interaction engine
   * @param headers Additional headers than can be passed to the request
   * @returns An observable of the customer data object T
   */
  public dispatch(request: InteractionRequest, headers = {}): Observable<CDO> {
    return this.call(request, headers).pipe(pluck<InteractionResponse<CDO>, CDO>('body'));
  }

  /**
   * Call sends an interaction request with any body and returns the expected body for that response
   * Call can also be used where you want to dispatch an action but get the whole interaction response
   * @param request The Action to set to the interaction engine
   * @param headers Additional headers than can be passed to the request
   * @returns An observable of the action result T
   */
  public call<T = CDO>(request: InteractionRequest, headers = {}): Observable<InteractionResponse<T>> {
    if (!this.actionMap.has(request.action)) {
      return throwError(new Error(`Action ${request.action} is not whitelisted in action map`));
    }
    const config = this.actionMap.get(request.action);

    this.sending$.next(true);
    return this.api.action<T>(request, headers).pipe(
      catchError(error => {
        this.sending$.next(false);
        return throwError(error);
      }),
      tap(interactionResponse => this.handleInteractionData(interactionResponse as InteractionResponse, config)),
    );
  }

  /**
   * Poll makes a call to the interaction engine where the expected response is a nav event
   * @param request The Action to set to the interaction engine
   * @param pollTime The time between polling attempts
   * @param attempts The total number of attempts to try
   * @param headers Additional headers than can be passed to the request
   */
  public poll<T = CDO>(
    request: InteractionRequest,
    pollTime = 5000,
    attempts = 10,
    headers = {},
  ): Observable<InteractionResponse<T>> {
    if (!this.actionMap.has(request.action)) {
      return throwError(new Error(`Action ${request.action} is not whitelisted in action map`));
    }
    const config = this.actionMap.get(request.action);
    if (config && !config.canPoll) {
      return throwError(`Action ${request.action} is not whitelisted to poll`);
    }

    let attempt = 0;

    return timer(0, pollTime).pipe(
      tap(() => attempt++),
      switchMap(_ => {
        if (attempt > attempts) {
          return throwError(new Error(`Unable to get action ${request.action} within ${attempts} attempts`));
        }
        return of(_);
      }),
      switchMap(_ => {
        this.sending$.next(true);
        return this.api.action<T>(request, headers).pipe(
          catchError(error => {
            this.sending$.next(false);
            return throwError(error);
          }),
          tap(response => this.handleInteractionData(response as InteractionResponse, config)),
          filter(response => typeof response.step !== 'undefined' && response.step.name !== this.nav.currentStep.name),
        );
      }),
      takeWhile(response => typeof response.step !== 'undefined' && response.step.name !== this.nav.nextStep.name),
    );
  }

  /**
   * Handle the interaction response
   * @param interaction The Interaction response
   * @param config An action configuration
   */
  private handleInteractionData(interaction: InteractionResponse<CDO>, config?: ActionConfig) {
    this.sending$.next(false);
    if (config && config.updateCdo && interaction.body) {
      this.cdo.set(interaction.body);
    }
    if (interaction.step) {
      this.nav.update(interaction.step);
    }
  }
}
