import { TestBed } from '@angular/core/testing';

import { FlowApiService } from './api-service.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ServiceConfig } from '../types/interaction';
import { InteractionResponse } from '../types/response';

describe('FlowApiServiceService', () => {
  let service: FlowApiService;
  let httpTestingController: HttpTestingController;

  const options: ServiceConfig = {
    apiRoot: '/gateway/api',
    apiVersion: 'v1',
    deploymentPath: 'interactions',
    servicePath: 'test-journey',
  };

  const response: InteractionResponse = {
    interactionId: 'test',
    actionErrors: [],
    additions: {},
    body: {},
    resumeCode: 'test-code',
    step: { name: 'test-step' },
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [FlowApiService],
    });
    service = TestBed.get(FlowApiService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should accept options to initialise the service', () => {
    service.init(options, 'test-interaction');

    expect(service.servicePath).toEqual('/gateway/api/test-journey/v1/interactions');
  });

  it('should support the call of an action', () => {
    service.init(options, 'api-interaction');
    service
      .action({
        action: 'call-action',
        body: {},
      })
      .subscribe(body => {
        expect(body.interactionId).toBe('test');
      });
    const req = httpTestingController.expectOne(
      '/gateway/api/test-journey/v1/interactions/api-interaction/actions/call-action',
    );
    expect(req.request.method).toEqual('POST');
    req.flush(response);
  });
});
