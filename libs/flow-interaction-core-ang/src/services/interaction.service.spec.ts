import { FlowInteractionService } from './interaction.service';
import { async, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ServiceConfig } from '../types/interaction';
import { InteractionResponse } from '../types/response';
import { FlowNavigationService } from './navigation.service';
import { createActionMap, createStepRouteMap } from '../utils/config';
import { FlowApiService } from './api-service.service';
import { skip } from 'rxjs/operators';
import { ACTION_MAP_FACTORY, FLOW_CONFIGURATION, STEP_MAP_FACTORY } from '../types/config';
import { FlowCustomerDataObjectService } from './customer-data-object.service';

describe('Flow Interaction Service', () => {
  let service: FlowInteractionService<CDO>;
  let httpTestingController: HttpTestingController;

  const options: ServiceConfig = {
    apiRoot: '/gateway/api',
    apiVersion: 'v1',
    deploymentPath: 'interactions',
    servicePath: 'test-journey',
  };

  const emailValue = 'test@example.com';

  interface CDO {
    firstName: string;
    lastName: string;
    email: string;
  }

  const response: InteractionResponse = {
    interactionId: 'test',
    actionErrors: [],
    additions: {},
    body: {
      firstName: 'Test',
      lastName: 'Entry',
      email: emailValue,
    },
    resumeCode: 'test-code',
    step: { name: 'test-step' },
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
        FlowNavigationService,
        FlowCustomerDataObjectService,
        FlowApiService,
        FlowInteractionService,
        {
          provide: FLOW_CONFIGURATION,
          useValue: {
            steps: {},
            actionConfig: {
              'call-action': {},
              'dispatch-action': {},
              'cdo-action': {
                updateCdo: true,
              },
            },
          },
        },
        {
          provide: STEP_MAP_FACTORY,
          useFactory: createStepRouteMap,
          deps: [FLOW_CONFIGURATION],
        },
        {
          provide: ACTION_MAP_FACTORY,
          useFactory: createActionMap,
          deps: [FLOW_CONFIGURATION],
        },
      ],
    });

    TestBed.get(FlowApiService);
    service = TestBed.get(FlowInteractionService);
    httpTestingController = TestBed.get(HttpTestingController);
  }));

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should support the dispatch of an action', () => {
    const body = {
      payload: {
        test: 'test-value',
      },
    };
    service.init(options, 'dispatch-interaction');
    service
      .dispatch({
        action: 'dispatch-action',
        body,
      })
      .subscribe(value => {
        expect(value.email).toBe(emailValue);
      });
    const req = httpTestingController.expectOne(
      '/gateway/api/test-journey/v1/interactions/dispatch-interaction/actions/dispatch-action',
    );
    expect(req.request.method).toEqual('POST');
    req.flush(response);
  });

  it('should support the call of an action', () => {
    service.init(options, 'call-interaction');
    service
      .call({
        action: 'call-action',
        body: {},
      })
      .subscribe(value => {
        expect(value).toBeTruthy();
      });
    const req = httpTestingController.expectOne(
      '/gateway/api/test-journey/v1/interactions/call-interaction/actions/call-action',
    );
    expect(req.request.method).toEqual('POST');
    req.flush({
      body: true,
    });
  });

  it('should set the customer data object', () => {
    const body = {
      payload: {
        test: 'new-action',
      },
    };
    service.init(options, 'cdo-interaction');

    service.cdo.data.pipe(skip(1)).subscribe(cdo => {
      expect(cdo.email).toEqual(emailValue);
    });

    service
      .dispatch({
        action: 'cdo-action',
        body,
      })
      .subscribe();
    const req = httpTestingController.expectOne(
      '/gateway/api/test-journey/v1/interactions/cdo-interaction/actions/cdo-action',
    );
    expect(req.request.method).toEqual('POST');
    req.flush(response);
  });

  it('should throw when accessing a non-white listed action', () => {
    const body = {
      payload: {
        test: 'some value',
      },
    };
    service.init(options, 'fake-interaction');

    service.dispatch({ action: 'non-action', body }).subscribe({
      error: err => {
        expect(err.message).toEqual(`Action non-action is not whitelisted in action map`);
      },
    });
  });
});
