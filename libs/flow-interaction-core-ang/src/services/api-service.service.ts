import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { InteractionRequest, InteractionName, ServiceConfig } from '../types/interaction';
import { InteractionResponse } from '../types/response';
import { pluck, tap } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class FlowApiService {
  /**
   * Current interaction name, set via the init method
   */
  private readonly interactionName$ = new BehaviorSubject<string>('');

  /**
   * The interaction ID
   */
  private readonly interactionId$ = new BehaviorSubject<string>('');

  /**
   * Service config for the instance of this service
   */
  private serviceConfig: ServiceConfig = {
    apiRoot: '',
    apiVersion: '',
    servicePath: '',
    deploymentPath: '',
  };

  constructor(private readonly http: HttpClient) {}

  /**
   * The full service path for the interaction service
   */
  public get servicePath() {
    const { apiRoot, servicePath, apiVersion, deploymentPath } = this.serviceConfig;
    let apiRootFinal = apiRoot;
    if (apiRoot.charAt(apiRoot.length - 1) === '/') {
      apiRootFinal = apiRoot.slice(0, -1);
    }
    const fullServicePath = `${apiRootFinal}/${servicePath}/${apiVersion}/${deploymentPath}`;
    return fullServicePath.replace(/\/+/, '/');
  }

  /**
   * Initialise the service with a service config
   * @param serviceConfig
   * @param interactionName
   */
  public init(serviceConfig: ServiceConfig, interactionName: InteractionName) {
    this.interactionId$.next('');
    this.serviceConfig = { ...this.serviceConfig, ...serviceConfig };
    this.interactionName$.next(interactionName);
  }

  /**
   * Internal action method, calls the interaction service
   * @param request
   * @param headers
   */
  public action<T = any>(request: InteractionRequest, headers = {}) {
    const body = {
      ...request.body,
    };
    if (!body.interactionId && this.interactionId$.value) {
      body.interactionId = this.interactionId$.value;
    }

    const uri = `${this.servicePath}/${this.interactionName$.value}/actions/${request.action}`;

    return this.http
      .request<InteractionResponse<T>>('post', uri, {
        body,
        headers,
        observe: 'response',
        responseType: 'json',
        withCredentials: false,
      })
      .pipe(
        pluck<HttpResponse<InteractionResponse<T>>, InteractionResponse<T>>('body'),
        tap(interaction => this.handleInteraction(interaction)),
      );
  }

  /**
   * Internal method to set the interaction id
   * @param interaction The Interaction Response
   */
  private handleInteraction<T>(interaction: InteractionResponse<T>) {
    if (!this.interactionId$.value && interaction.interactionId) {
      this.interactionId$.next(interaction.interactionId);
    }
  }
}
