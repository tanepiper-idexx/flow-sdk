import { TestBed } from '@angular/core/testing';

import { FlowCustomerDataObjectService } from './customer-data-object.service';
import { skip } from 'rxjs/operators';

export interface TestCDO {
  name: string;
  age: number;
}

describe('CustomerDataObjectService', () => {
  let service: FlowCustomerDataObjectService<TestCDO>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FlowCustomerDataObjectService],
    });

    service = TestBed.get(FlowCustomerDataObjectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should allow the customer data object to be set', () => {
    service.data.pipe(skip(1)).subscribe(data => {
      expect(data.name).toBe('Test 1');
    });

    service.set({
      name: 'Test 1',
      age: 18,
    });
  });

  it('should allow the customer data to be plucked', () => {
    service
      .get('age')
      .pipe(skip(1))
      .subscribe(age => {
        expect(age).toEqual(25);
      });

    service.set({
      name: 'Test 2',
      age: 25,
    });
  });
});
