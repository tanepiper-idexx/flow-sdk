import { FlowNavigationService } from './navigation.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { createActionMap, createStepRouteMap } from '../utils/config';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, NgZone } from '@angular/core';
import { Routes } from '@angular/router';
import { Location } from '@angular/common';
import { ACTION_MAP_FACTORY, FLOW_CONFIGURATION, STEP_MAP_FACTORY } from '../types/config';

describe('Flow Interaction: Navigation Service', () => {
  let service: FlowNavigationService;
  let location: Location;
  let fixture: ComponentFixture<TestComponent>;
  let component: TestComponent;

  @Component({
    template: '',
    changeDetection: ChangeDetectionStrategy.OnPush,
  })
  class TestComponent {
    constructor(public navigation: FlowNavigationService, public ref: ChangeDetectorRef, public zone: NgZone) {}
  }

  @Component({
    template: '',
  })
  class TestView {}

  const routes: Routes = [
    {
      path: '',
      component: TestView,
    },
    {
      path: 'test-1',
      component: TestView,
    },
    {
      path: 'test-2',
      component: TestView,
    },
    {
      path: 'test-path',
      component: TestView,
    },
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes)],
      declarations: [TestComponent, TestView],
      providers: [
        {
          provide: FlowNavigationService,
          useClass: FlowNavigationService,
        },
        {
          provide: FLOW_CONFIGURATION,
          useValue: {
            steps: {
              'test-1': ['test-1'],
              'test-2': ['test-2'],
              'test-path': '/test-path',
            },
            actionConfig: {},
          },
        },
        {
          provide: STEP_MAP_FACTORY,
          useFactory: createStepRouteMap,
          deps: [FLOW_CONFIGURATION],
        },
        {
          provide: ACTION_MAP_FACTORY,
          useFactory: createActionMap,
          deps: [FLOW_CONFIGURATION],
        },
      ],
    }).compileComponents();

    service = TestBed.get(FlowNavigationService);
    location = TestBed.get(Location);
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.debugElement.componentInstance;

    fixture.detectChanges();
  }));

  it('should define the service', () => {
    expect(service).toBeDefined();
  });

  it('should give a list of steps', () => {
    expect(service.steps.length).toBe(3);
  });

  it('should return a route for a step', () => {
    expect(service.getRoute('test-1')).toContain('test-1');
  });

  it('should go to the next step', async(async () => {
    service.nextStep = { name: 'test-2' };
    expect(service.nextStep.name).toBe('test-2');
    await service.next();
    expect(location.path()).toBe('/test-2');
  }));

  it('should support string paths', async(async () => {
    service.nextStep = { name: 'test-path' };
    expect(service.nextStep.name).toBe('test-path');
    await service.next();
    expect(location.path()).toBe('/test-path');
  }));

  it('should give the current step', () => {
    service.currentStep = { name: 'test-2' };
    expect(service.currentStep.name).toBe('test-2');
  });

  it('should reject on no step', async () => {
    service.nextStep = { name: 'test-3' };
    try {
      await service.next();
    } catch (exp) {
      expect(exp).toBe(`No route for test-3`);
    }
  });

  it('should only update current status if status changes', async () => {
    service.currentStep = { name: 'test-1', status: 'NOT_FINISHED' };
    service.update({ name: 'test-1' });
    await service.next();
    expect(service.currentStep.status).toEqual('NOT_FINISHED');
    service.update({ name: 'test-1', status: 'FINISHED' });
    await service.next();
    expect(service.currentStep.status).toEqual('FINISHED');
  });

  it('should change to the next step', async () => {
    service.currentStep = { name: 'test-1', status: 'NOT_FINISHED' };
    service.update({ name: 'test-2' });
    await service.next();
    expect(service.currentStep.name).toEqual('test-2');
  });
});
