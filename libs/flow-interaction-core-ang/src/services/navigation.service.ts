import { Inject, Injectable, NgZone } from '@angular/core';
import { RoutePath, Step, StepName } from '../types/navigation';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { StepMap } from '../utils/config';
import { STEP_MAP_FACTORY } from '../types/config';

/**
 * The Flow Navigation Service handles the step and status of the application
 * via the nav options provided by Interaction.
 */
@Injectable()
export class FlowNavigationService {
  /**
   * Store the current step
   */
  private readonly currentStep$ = new BehaviorSubject<Step>({ name: '' });

  /**
   * Store the next step
   */
  private readonly nextStep$ = new BehaviorSubject<Step>({ name: '' });

  constructor(
    @Inject(STEP_MAP_FACTORY) private readonly navigatorMap: StepMap,
    private readonly router: Router,
    private readonly zone: NgZone,
  ) {}

  /**
   * The current step the navigator service is on
   */
  public get currentStep(): Step {
    return this.currentStep$.value;
  }

  /**
   * The current step the navigator service is on
   * @param step The step to set
   */
  public set currentStep(step: Step) {
    this.currentStep$.next(step);
  }

  /**
   * Returns the next step
   */
  public get nextStep(): Step {
    return this.nextStep$.value;
  }

  /**
   * The current step the navigator service is on
   * @param step The step to set
   */
  public set nextStep(step: Step) {
    this.nextStep$.next(step);
  }

  /**
   * Get a list of all the step keys from the navigator map
   */
  public get steps(): string[] {
    return Array.from(this.navigatorMap.keys());
  }

  /**
   * Return a route from the navigator map
   * @param step The step to load
   */
  public getRoute(step: StepName): RoutePath | undefined {
    return this.navigatorMap.get(step);
  }

  /**
   * Call this when you have finished with a dispatch or call to an action
   * If a new step has been given the application with navigate, otherwise
   * a status will be returned instead to be dealt with
   */
  public next(): Promise<boolean> {
    return this.goto(this.nextStep$.value);
  }

  /**
   * Go to a nav route via a step
   * @param step The step to navigate to
   */
  private goto(step: Step): Promise<boolean> {
    const route = this.getRoute(step.name) as RoutePath;
    if (!route) {
      return Promise.reject(`No route for ${step.name}`);
    }
    const commands = Array.isArray(route) ? route : [route];
    return this.zone.run(() => this.router.navigate(commands)).then(() => {
      this.currentStep$.next(step);
      return Promise.resolve(true) as Promise<any>;
    });
  }

  /**
   * Update the current step with status, or set the next step
   * @param step The step that will be set, including name and status
   */
  public update(step: Step) {
    if ((step.name === this.currentStep.name && step.status) || step.name !== this.currentStep.name) {
      this.nextStep$.next(step);
    } else {
      this.nextStep$.next(this.currentStep$.value);
    }
  }
}
