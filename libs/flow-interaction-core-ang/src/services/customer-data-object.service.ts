import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { pluck } from 'rxjs/operators';
import { CustomerDataObject } from '../types/interaction';

/**
 * Internal service for handling the customer data object
 */
@Injectable()
export class FlowCustomerDataObjectService<CDO = CustomerDataObject> {
  /**
   * Customer Data Object
   */
  private readonly customerData$ = new BehaviorSubject<Partial<CDO>>({});

  /**
   * Get an observable value of the customer data object
   */
  public get data(): Observable<Partial<CDO>> {
    return this.customerData$.asObservable();
  }

  /**
   * Key a value of a key property as an observable value
   * @param key The key of the property
   * @returns The observable value of the property get
   */
  public get<T>(key: string): Observable<T> {
    return this.data.pipe(pluck<Partial<CDO>, T>(key));
  }

  /**
   * Set the data from the customer data object
   * @param cdo The partial of the customer data object
   */
  public set(cdo: Partial<CDO>): void {
    this.customerData$.next(cdo);
  }
}
