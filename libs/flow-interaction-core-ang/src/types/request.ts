/**
 * An interaction request
 */
import { Customizable, InteractionID } from './interaction';

export type InteractionRequestPayload = any;

export interface InteractionBody<T = InteractionRequestPayload> extends Customizable {
  /**
   * The interaction ID of the request
   */
  interactionId?: InteractionID;
  /**
   * The payload to pass
   */
  payload?: T;
}
