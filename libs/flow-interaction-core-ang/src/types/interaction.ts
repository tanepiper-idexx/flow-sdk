import { InteractionBody } from './request';

/**
 * A type representing the InteractionID
 */
export type InteractionID = string;

/**
 * The name of the interaction
 */
export type InteractionName = string;

/**
 * Type for an interaction resume code
 */
export type InteractionResumeCode = string;

/**
 * The name of the action
 */
export type ActionName = string;

/**
 * Generic Type for customer data object
 */
export interface CustomerDataObject {
  [key: string]: any;
}

/**
 * The payload to send a flow interaction
 */
export interface InteractionRequest<T = InteractionBody> {
  /**
   * The action to be sent
   */
  action: ActionName;
  /**
   * The Interaction body
   */
  body: T;
}

/**
 * A service configuration for the flow interaction service
 */
export interface ServiceConfig {
  /**
   * The API root of the server
   */
  apiRoot: string;
  /**
   * The API version being used
   */
  apiVersion: string;
  /**
   * The service path of the flow interaction engine
   */
  servicePath: string;
  /**
   * The deployment path of the flow interaction engine
   */
  deploymentPath: string;
}

/**
 * Additions allowed on a customizable model
 */
export interface Additions {
  /**
   * A Key with any value type
   */
  [key: string]: any;
}

/**
 * Customizable interface
 */
export interface Customizable {
  /**
   * Optional additions
   */
  additions?: Additions;
}
