/**
 * Type for a step name
 */
export type StepName = string;

/**
 * type for a step status
 */
export type StepStatus = string;

/**
 * Type for a step
 */
export interface Step {
  /**
   * The name of the step
   */
  name: StepName;
  /**
   * Optional step status
   */
  status?: StepStatus;
}

/**
 * The path of a step for Angular routing
 */
export type RoutePath = string | string[];

/**
 * The configuration of a step
 */
export interface StepConfiguration {
  /**
   * Name of a step
   */
  name: StepName;
  /**
   * Angular route for step
   */
  path: RoutePath;

  /**
   * An existing step config to override
   */
  override?: StepName;
}
