import { RoutePath, StepName } from './navigation';
import { ActionName, CustomerDataObject } from './interaction';
import { InjectionToken } from '@angular/core';

/**
 * A Step Map configuration provides a key as a step name and a route path as configuration
 */
export interface StepConfigMap {
  /**
   * A Step name and path
   */
  [stepName: string]: RoutePath;
}

export interface ActionConfig<T = CustomerDataObject> {
  type: T;
  /**
   * Property if called to update the CDO
   */
  updateCdo?: boolean;

  /**
   * If the action can be polled
   */
  canPoll?: boolean;
}

/**
 * The action configuration allows actions to be white listed
 */
export interface ActionConfigMap {
  [actionName: string]: any;
}

/**
 * The Flow Core configuration
 */
export interface FlowInteractionConfig {
  /**
   * Step Configuration
   */
  steps: StepConfigMap;
  /**
   * Action Configuration
   */
  actionConfig: ActionConfigMap;
}

/**
 * A Step tuple
 */
export type StepTuple = [StepName, RoutePath];

/**
 * A Action tuple
 */
export type ActionTuple = [ActionName, ActionConfig];

/**
 * Factory provider to create the step tuple
 */
export const STEP_MAP_FACTORY = new InjectionToken<StepTuple>('STEP_MAP_FACTORY');

/**
 * Factory provider to create the Action tuple
 */
export const ACTION_MAP_FACTORY = new InjectionToken<ActionTuple>('ACTION_MAP_FACTORY');

/**
 * Flow Core Config injector
 */
export const FLOW_CONFIGURATION = new InjectionToken<FlowInteractionConfig[]>('FLOW_CONFIGURATION');
