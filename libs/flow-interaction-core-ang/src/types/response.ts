import { Step } from './navigation';

/**
 * An interaction Response
 */
import { Customizable, InteractionID, InteractionResumeCode } from './interaction';

/**
 * A body returned from the interaction response
 */
export type InteractionRequestBody = any;

/**
 * An interaction response with body of T
 */
export interface InteractionResponse<T = InteractionRequestBody> extends Customizable {
  /**
   * The interaction ID
   */
  interactionId: InteractionID;
  /**
   * The next step to go to, or current step
   */
  step?: Step;
  /**
   * The resume code for the interaction
   */
  resumeCode?: InteractionResumeCode;
  /**
   * The body sent with the response
   */
  body?: T;
  /**
   * Any action errors provided by the interaction engine
   */
  actionErrors?: ActionError[];
}

/**
 * Action Error Type
 */
export interface ActionError extends Customizable {
  /**
   * An action error code
   */
  code: string;
  /**
   * An action error message
   */
  message: string;
}
