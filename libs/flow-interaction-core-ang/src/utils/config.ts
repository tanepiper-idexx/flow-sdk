import { RoutePath, StepName } from '../types/navigation';
import { ActionName } from '../types/interaction';
import { ActionConfig, ActionTuple, FlowInteractionConfig, StepTuple } from '../types/config';

/**
 * A Step Map container a step name as a key, which maps to an Angular route path or route string
 */
export type StepMap = Map<StepName, RoutePath>;

/**
 * A Step Map container a step name as a key, which maps to an Angular route path or route string
 */
export type ActionMap = Map<ActionName, ActionConfig>;

/**
 * Internal method to create step map, it takes an array of tuples and creates a new
 * tuple, allowing for new steps or overriding of steps
 * @param stepConfigurations
 */
export function createStepRouteMap(stepConfigurations: FlowInteractionConfig): StepMap {
  let finalTuple: StepTuple[] = [];
  Object.entries(stepConfigurations.steps).forEach(([step, route]) => {
    finalTuple = [...finalTuple, [step, route]];
  });
  return new Map<StepName, RoutePath>(finalTuple);
}

export function createActionMap(stepConfigurations: FlowInteractionConfig): ActionMap {
  let finalTuple: ActionTuple[] = [];
  Object.entries(stepConfigurations.actionConfig).forEach(([action, config]) => {
    finalTuple = [...finalTuple, [action, config]];
  });
  return new Map<StepName, ActionConfig>(finalTuple);
}
