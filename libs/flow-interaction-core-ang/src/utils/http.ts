export interface HttpParameters {
  [key: string]: string | number | boolean | any[] | undefined;
}

export interface NormalisedHttpParameters {
  [key: string]: string | string[];
}

/**
 * Reducer for a single parameter to convert to a string or boolean, if the value is undefined
 * the key is not set
 * @param accum
 * @param key
 * @param value
 * @returns The HTTP Parameter object with the key/value set
 */
export const normalizeHttpParameter = (
  accum: HttpParameters,
  [key, value]: [string, any],
): NormalisedHttpParameters => {
  if (value === undefined) {
    return accum as NormalisedHttpParameters;
  }
  if (Array.isArray(value)) {
    return Object.assign({}, accum, {
      [key]: value.map(v => String(v)),
    });
  } else {
    return Object.assign({}, accum, { [key]: String(value) });
  }
};

/**
 * Method to normalise a set of HTTP parameters as a object
 * @param params The HTTP parameters object
 * @returns A Normalised HTTP Parameters object
 */
export const normalizeHttpParameters = (params: HttpParameters = {}): NormalisedHttpParameters =>
  Object.entries(params).reduce(normalizeHttpParameter, {});
