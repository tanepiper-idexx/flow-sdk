import { createActionMap, createStepRouteMap } from './config';
import { FlowInteractionConfig } from '../types/config';

describe('Flow Config factory', () => {
  it('should support the creation of step map from a config', () => {
    const config: FlowInteractionConfig = {
      steps: {
        'step-1': ['path-1'],
      },
      actionConfig: {},
    };

    const map = createStepRouteMap(config);
    expect(map.size).toBe(1);
  });

  it('should support multiple steps in a config', () => {
    const config: FlowInteractionConfig = {
      steps: {
        'step-1': ['path-1'],
        'step-2': ['path-2'],
      },
      actionConfig: {},
    };
    const map = createStepRouteMap(config);
    expect(map.size).toBe(2);
  });

  it('should support the creation of action from a config', () => {
    const config: FlowInteractionConfig = {
      steps: {},
      actionConfig: {
        'action-1': {},
      },
    };

    const map = createActionMap(config);
    expect(map.size).toBe(1);
  });

  it('should support the creation of multiple action from a config', () => {
    const config: FlowInteractionConfig = {
      steps: {},
      actionConfig: {
        'action-1': {},
        'action-2': {},
      },
    };

    const map = createActionMap(config);
    expect(map.size).toBe(2);
  });
});
