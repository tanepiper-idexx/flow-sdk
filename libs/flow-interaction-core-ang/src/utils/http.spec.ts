import { normalizeHttpParameters } from './http';

describe('Http Utilities', () => {
  it('should normalise an string property', () => {
    const input = {
      testKey: 'test-1',
    };
    const params = normalizeHttpParameters(input);
    expect(params.testKey).toEqual('test-1');
  });

  it('should normalise a undefined property', () => {
    const input = {
      testKey: 'test-2',
      testKey2: undefined,
    };
    const params = normalizeHttpParameters(input);
    expect(params.testKey2).toBeUndefined();
  });

  it('should normalise a number property', () => {
    const input = {
      testKey: 'test-3',
      testKey2: 5,
    };
    const params = normalizeHttpParameters(input);
    expect(params.testKey2).toBe('5');
  });

  it('should normalise a number property', () => {
    const input = {
      testKey: 'test-4',
      testKey2: true,
      testKey3: false,
    };
    const params = normalizeHttpParameters(input);
    expect(params.testKey2).toBe('true');
    expect(params.testKey3).toBe('false');
  });

  it('should normalise an array of values', () => {
    const input = {
      testKey: 'test-5',
      testKey2: ['a', 5, true],
    };
    const params = normalizeHttpParameters(input);
    expect(params.testKey2).toEqual(['a', '5', 'true']);
  });
});
