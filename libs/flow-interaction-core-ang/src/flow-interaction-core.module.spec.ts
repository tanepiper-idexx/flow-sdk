import { TestBed } from '@angular/core/testing';
import { FlowInteractionCoreModule } from './flow-interaction-core.module';

describe('Flow Interaction Core Module', function() {
  it('should create a module when imported', () => {
    TestBed.configureTestingModule({
      imports: [FlowInteractionCoreModule],
    });
    const module = TestBed.get(FlowInteractionCoreModule);
    expect(module).toBeDefined();
  });

  it('should create a module when imported with a forRoot method', () => {
    TestBed.configureTestingModule({
      imports: [
        FlowInteractionCoreModule.forRoot({
          actionConfig: {},
          steps: {},
        }),
      ],
    });
    const module = TestBed.get(FlowInteractionCoreModule);
    expect(module).toBeDefined();
  });
});
