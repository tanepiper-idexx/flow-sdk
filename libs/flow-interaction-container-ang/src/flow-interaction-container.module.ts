import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { FlowInteractionContainerComponent } from './flow-interaction-container.component';

@NgModule({
  declarations: [FlowInteractionContainerComponent],
  imports: [
    CommonModule,
    BackbaseCoreModule.withConfig({
      classMap: { FlowInteractionContainerComponent },
    }),
  ],
})
export class FlowInteractionContainerModule {}
