import { Component, Inject, Input, OnInit } from '@angular/core';
import { FlowInteractionService, InteractionName } from '@backbase/flow-interaction-core-ang';
import { PAGE_CONFIG, PageConfig } from '@backbase/foundation-ang/web-sdk';
import { RoutableContainer } from '@backbase/foundation-ang/core';

@RoutableContainer()
@Component({
  selector: 'bb-flow-interaction-container',
  template: `
      <bb-area></bb-area>
      <bb-router-outlet></bb-router-outlet>`,
  providers: [FlowInteractionService],
})
export class FlowInteractionContainerComponent implements OnInit {
  /**
   * The Journey name to enable within this container
   */
  @Input() interactionName: InteractionName = '';

  /**
   * The API version to use
   */
  @Input() apiVersion = '';

  /**
   * The service path of the interaction
   */
  @Input() servicePath = '';

  /**
   * The deployment path of the interaction
   */
  @Input() deploymentPath = '';

  constructor(
    @Inject(PAGE_CONFIG) private readonly pageConfig: PageConfig,
    public readonly flow: FlowInteractionService,
  ) {}

  ngOnInit() {
    this.flow.init(
      {
        servicePath: this.servicePath,
        deploymentPath: this.deploymentPath,
        apiVersion: this.apiVersion,
        apiRoot: this.pageConfig.apiRoot,
      },
      this.interactionName,
    );
  }
}
