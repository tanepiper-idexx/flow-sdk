import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FlowInteractionContainerComponent } from './flow-interaction-container.component';
import { FlowInteractionCoreModule } from '@backbase/flow-interaction-core-ang';
import { BackbaseCoreModule, ItemModel } from '@backbase/foundation-ang/core';

import { WebSdkApiModule, PAGE_CONFIG } from '@backbase/foundation-ang/web-sdk';
import { RouterTestingModule } from '@angular/router/testing';

describe('Flow Interaction Container', function() {
  let component: FlowInteractionContainerComponent;
  let fixture: ComponentFixture<FlowInteractionContainerComponent>;

  beforeEach(async(() => {
    const module = TestBed.configureTestingModule({
      imports: [
        FlowInteractionCoreModule,
        RouterTestingModule,
        WebSdkApiModule,
        BackbaseCoreModule.withConfig({
          classMap: {
            FlowInteractionContainerComponent,
          },
        }),
      ],
      declarations: [FlowInteractionContainerComponent],
      providers: [
        {
          provide: PAGE_CONFIG,
          useValue: {
            apiRoot: '/gateway/api/',
          },
        },
        {
          provide: ItemModel,
          useValue: {},
        },
      ],
    });

    module.compileComponents();

    fixture = TestBed.createComponent(FlowInteractionContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should call the flow service on ngInit', () => {
    spyOn(component.flow, 'init');
    component.ngOnInit();
    expect(component.flow.init).toHaveBeenCalled();
  });
});
